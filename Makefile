BLACK          := black
COVERAGE       := coverage
PYTEST         := pytest

.PHONY: test
test:
	$(COVERAGE) run -m $(PYTEST) tests/
	$(COVERAGE) report

.PHONY: format
format:
	$(BLACK) src/ tests/ setup.py
