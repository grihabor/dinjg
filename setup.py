from setuptools import setup, find_packages

setup(
    name="dinjg",
    author="Borodin Gregory",
    author_email="grihabor@gmail.com",
    packages=find_packages("src"),
    url="https://gitlab.com/grihabor/dinjg/",
    package_dir={"": "src"},
    install_requires=["typed_ast", "click", "graphviz"],
    entry_points={"console_scripts": ["dinjg = dinjg.cli:main"]},
    extras_require={"develop": ["pytest", "coverage"]},
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
)
