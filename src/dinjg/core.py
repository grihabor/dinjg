import importlib.util
import importlib
import inspect
import os
import pkgutil
from importlib.machinery import FileFinder, SourceFileLoader
from pprint import pformat
from typing import Iterable, List, Dict, Tuple, NamedTuple
import html

import click
import enum
from typed_ast import ast3
import graphviz

from dinjg.diparser import get_di_dependencies


def get_mro_dependencies(class_def: ast3.ClassDef, module):
    class_name = class_def.name
    target_cls = getattr(module, class_name)
    return [
        cls.__name__
        for cls in target_cls.__mro__
        if cls is not target_cls and cls is not object
    ]


class ClassType(enum.Enum):
    abstract = "abstract"
    regular = "regular"


class Node(NamedTuple):
    name: str
    docstring: str
    class_type: ClassType


def _collect_nodes(package_name: str) -> List[Node]:
    name_and_module = (
        (ast_class_def.name, module)
        for ast_class_def, module in iter_class_def(package_name)
    )
    name_and_cls_obj = (
        (class_name, getattr(module, class_name))
        for class_name, module in name_and_module
    )

    return [
        Node(
            name=class_name,
            docstring=inspect.getdoc(cls_obj),
            class_type=(
                ClassType.abstract if inspect.isabstract(cls_obj) else ClassType.regular
            ),
        )
        for class_name, cls_obj in name_and_cls_obj
    ]


class DependencyType(enum.Enum):
    dinj = "dinj"
    dmro = "dmro"


class Edge(NamedTuple):
    start: str
    end: str
    dtype: DependencyType


def _collect_edges(package_name: str):
    edges = []
    for ast_class_def, module in iter_class_def(package_name):
        name, dependencies = ast_class_def.name, get_di_dependencies(ast_class_def)
        if not dependencies:
            continue

        edges.extend(Edge(name, d, DependencyType.dinj) for d in dependencies)

    for ast_class_def, module in iter_class_def(package_name):
        name, dependencies = (
            ast_class_def.name,
            get_mro_dependencies(ast_class_def, module),
        )
        if not dependencies:
            continue

        edges.extend(Edge(name, d, DependencyType.dmro) for d in dependencies)
    return edges


def _main(package_name: str):
    nodes, edges = _collect_nodes(package_name), _collect_edges(package_name)
    return build_graph(nodes, edges)


def _render_label(text, docstring):
    return """<
  <table border='1' cellborder='0' style='rounded'>
    <tr><td>{text}</td></tr>
    <tr><td>{docstring}</td></tr>
  </table>
>""".format(
        text=html.escape(text), docstring=html.escape(docstring) if docstring is not None else ''
    )


def build_graph(nodes: List[Node], edges: List[Edge]):
    click.echo(pformat(nodes), err=True)
    graph = graphviz.Digraph()
    graph.graph_attr["overlap"] = "false"

    for node in nodes:
        graph.node(
            node.name, shape="plaintext", label=_render_label(node.name, docstring=node.docstring)
        )  # {True: "box", False: "oval"}.get(cls_type, "oval"))

    for edge in edges:
        graph.edge(
            edge[0],
            edge[1],
            **(
                dict(arrowhead="dot", dir="both", arrowtail="oinv")
                if edge.dtype == DependencyType.dmro
                else dict()
            )
        )

    click.echo(graph.source)


def iter_class_def(package_name: str):
    module = importlib.import_module(package_name)
    path = module.__path__
    for module in walk_packages(path, prefix=module.__name__ + "."):
        ast_module = _parse(module)
        for ast_class_def in _iter_class_def(ast_module):
            yield ast_class_def, module


def _get_source(module):
    sourcefile = inspect.getsourcefile(module)
    if os.path.isdir(sourcefile):
        targetfile = os.path.join(sourcefile, "__init__.py")
    else:
        targetfile = sourcefile
    with open(targetfile, "rb") as f:
        return f.read(), targetfile


def _parse(module):
    source, path = _get_source(module)
    return ast3.parse(source, filename=os.path.basename(path))


def _iter_class_def(module: ast3.Module) -> Iterable[ast3.ClassDef]:
    for ast_obj in module.body:
        if isinstance(ast_obj, ast3.ClassDef):
            yield ast_obj


def walk_packages(path: List[str], prefix: str) -> Iterable:
    spec = importlib.util.spec_from_file_location(
        prefix.strip("."), location=path[0], loader=FileFinder(path[0])
    )
    module = importlib.util.module_from_spec(spec)
    yield module
    for info in pkgutil.iter_modules(path, prefix=prefix):
        module_finder = info.module_finder  # type: FileFinder
        loader = module_finder.find_loader(info.name)[0]  # type: SourceFileLoader
        submodule = loader.load_module(info.name)
        if not hasattr(submodule, "__path__"):
            yield submodule
            continue
        yield from walk_packages(submodule.__path__, prefix=info.name + ".")
