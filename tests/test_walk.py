import os

from dinjg.core import walk_packages


def test_walk_packages():
    tests_path = os.path.dirname(os.path.abspath(__file__))
    dummy_package_root = os.path.join(tests_path, "data", "dummy")
    packages = list(walk_packages([dummy_package_root], prefix="dummy."))
    assert {p.__name__ for p in packages} == {
        "dummy",
        "dummy.abc",
        "dummy.none_getter",
        "dummy.user",
        "dummy.user.getter_user",
    }
