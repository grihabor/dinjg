import pytest
from typed_ast import ast3

from dinjg.core import _iter_class_def
from dinjg.diparser import get_di_dependencies


@pytest.mark.parametrize(
    "source",
    [
        b"""
class Owner:
    def __init__(
        getter,  # type: GetterInterface
        setter,  # type: SetterInterface
    ):
        self._getter = getter
        self._setter = setter
""",
        b"""
class Owner:
    def __init__(
        getter: GetterInterface,
        setter: SetterInterface,
    ):
        self._getter = getter
        self._setter = setter
""",
        pytest.param(
            b"""
class Owner:
    def __init__(getter, setter):
        # type: (GetterInterface, SetterInterface) -> None
        self._getter = getter
        self._setter = setter
""",
            marks=pytest.mark.xfail(reason="Not supported"),
        ),
    ],
)
def test_get_class_dependencies(source):
    ast_module = ast3.parse(source)
    class_def_list = list(_iter_class_def(ast_module))
    assert len(class_def_list) == 1
    class_def = class_def_list[0]
    dependencies = get_di_dependencies(class_def)
    assert dependencies == {"GetterInterface", "SetterInterface"}


@pytest.mark.parametrize(
    "source",
    [
        b"""
class Owner:
    def __init__(
        getter,  # type: List[GetterInterface]
        setter,  # type: Dict[str, SetterInterface]
    ):
        self._getter = getter
        self._setter = setter
""",
        b"""
class Owner:
    def __init__(
        getter: List[GetterInterface],
        setter: Dict[str, SetterInterface],
    ):
        self._getter = getter
        self._setter = setter
""",
        pytest.param(
            b"""
class Owner:
    def __init__(getter, setter):
        # type: (List[GetterInterface], Dict[str, SetterInterface]) -> None
        self._getter = getter
        self._setter = setter
""",
            marks=pytest.mark.xfail(reason="Not supported"),
        ),
    ],
)
def test_get_class_dependencies_complex(source):
    ast_module = ast3.parse(source)
    class_def_list = list(_iter_class_def(ast_module))
    assert len(class_def_list) == 1
    class_def = class_def_list[0]
    dependencies = get_di_dependencies(class_def)
    assert dependencies == {"GetterInterface", "SetterInterface", "List", "Dict", "str"}
