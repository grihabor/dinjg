import io
import os
import re
import shutil
import sys
from contextlib import redirect_stdout

import pytest
from dinjg.core import _main


@pytest.fixture
def dummy_package(tmpdir):
    tests_path = os.path.dirname(os.path.abspath(__file__))
    dummy_package_root = os.path.join(tests_path, "data", "dummy")
    shutil.copytree(dummy_package_root, tmpdir / "dummy")
    yield tmpdir


def test_dinjg(dummy_package):
    sys.path = sys.path + [str(dummy_package)]
    stdout = io.BytesIO()
    with redirect_stdout(stdout):
        _main("dummy")
    actual = stdout.getvalue().decode("utf-8")
    regex = re.compile(r" *\[[^]]*\]", re.DOTALL)
    actual_modified = regex.sub("", actual)
    expected = """digraph {
\tgraph
\tGetterInterface
\tNoneGetter
\tGetterUser
\tGetterUser -> GetterInterface
\tGetterInterface -> ABC
\tNoneGetter -> GetterInterface
\tNoneGetter -> ABC
}
"""
    assert actual_modified == expected
