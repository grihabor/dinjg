import abc


class GetterInterface(abc.ABC):
    @abc.abstractmethod
    def get(self):
        raise NotImplementedError
