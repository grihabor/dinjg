from dummy.abc import GetterInterface


class NoneGetter(GetterInterface):
    def get(self):
        return None
